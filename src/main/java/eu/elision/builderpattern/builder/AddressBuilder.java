package eu.elision.builderpattern.builder;

import eu.elision.builderpattern.Address;

public class AddressBuilder {

    private String street;
    private String number;
    private String city;
    private String zipcode;

    private AddressBuilder() {
    }

    public static AddressBuilder anAddressBuilder() {
        return new AddressBuilder();
    }

    public Address build() {
        return new Address(street, number, city, zipcode);
    }

    public AddressBuilder withStreet(String street) {
        this.street = street;
        return this;
    }

    public AddressBuilder withNumber(String number) {
        this.number = number;
        return this;
    }

    public AddressBuilder withCity(String city) {
        this.city = city;
        return this;
    }

    public AddressBuilder withZipcode(String zipcode) {
        this.zipcode = zipcode;
        return this;
    }
}
