package eu.elision.builderpattern.builder;

import eu.elision.builderpattern.Address;
import eu.elision.builderpattern.Person;

import java.util.ArrayList;
import java.util.List;

public class PersonBuilder {

    private String firstname;
    private String lastname;
    private String email;
    private Address address;
    private List<String> phoneNumbers;

    //Private constructor to make sure nobody can take an instance
    private PersonBuilder() {
        this.phoneNumbers = new ArrayList<String>();
    }

    public static PersonBuilder aPersonBuilder() {
        return new PersonBuilder();
    }

    public Person build() {
        return new Person(firstname, lastname, email, address, phoneNumbers);
    }

    public PersonBuilder withFirstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public PersonBuilder withLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public PersonBuilder withEmail(String email) {
        this.email = email;
        return this;
    }

    public PersonBuilder withAddress(Address address) {
        this.address = address;
        return this;
    }

    public PersonBuilder withPhoneNumbers(List<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
        return this;
    }

    public PersonBuilder addPhoneNumber(String phoneNumber) {
        phoneNumbers.add(phoneNumber);
        return this;
    }
}
