package eu.elision.builderpattern;

import edu.emory.mathcs.backport.java.util.Collections;

import java.util.List;

/**
 * @Author Joris Cryns
 * <p>
 * Example Person class.
 * <p>
 * When using a Builder Pattern, make your fields final and your object immutable.
 */

public class Person {

    private final String firstname;
    private final String lastname;
    private final String email;
    private final Address address;
    private final List<String> phoneNumbers;

    public Person(String firstname, String lastname, String email, Address address, List<String> phoneNumbers) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.address = address;
        //If you don't use unmodifiableList you can still change the values, however not the length of the list.
        this.phoneNumbers = Collections.unmodifiableList(phoneNumbers);

    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public Address getAddress() {
        return address;
    }

    public List<String> getPhoneNumbers() {
        return phoneNumbers;
    }
}
