package eu.elision.builderpattern;

public class Address {

    private final String street;
    private final String number;
    private final String city;
    private final String zipcode;

    public Address(String street, String number, String city, String zipcode) {
        this.street = street;
        this.number = number;
        this.city = city;
        this.zipcode = zipcode;
    }

    public String getStreet() {
        return street;
    }

    public String getNumber() {
        return number;
    }

    public String getCity() {
        return city;
    }

    public String getZipcode() {
        return zipcode;
    }
}
