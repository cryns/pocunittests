package eu.elision.poc;

import java.math.BigDecimal;

/**
 * @author crynsjo
 *         Examples for possible Utils class
 */
public class BigDecimalUtils {

    private BigDecimalUtils() {
    }

    /**
     * <p>Checks whether BigDecimal is zero or null
     *
     * @param bd BigDecimal to be tested
     * @return boolean
     */
    public static boolean isZero(BigDecimal bd) {
        return bd == null || (bd.compareTo(BigDecimal.ZERO) == 0);
    }

    /**
     * <p>Checks whether BigDecimal is not zero of null
     *
     * @param bd BigDecimal to be tested
     * @return boolean
     */
    public static boolean isNotZero(BigDecimal bd) {
        return !isZero(bd);
    }

    /**
     * <p>Going to round the BigDecimal with the HalfUp strategy</p>
     *
     * @param bd       BigDecimal to round up
     * @param decimals Number of decimals
     * @return BigDecimal
     */
    public static BigDecimal roundHalfUp(BigDecimal bd, int decimals) {
        return bd.setScale(decimals, BigDecimal.ROUND_HALF_UP);
    }
}
