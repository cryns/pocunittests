package eu.elision.poc;

import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Proof that the constructor way can cause problems.
 * If you're working with prices this could be a real problem.
 * <p>
 * <a href="http://stackoverflow.com/questions/7186204/bigdecimal-to-use-new-or-valueof" >Explanation</a>
 */
public class BigDecimalCalculationTests {

    @Test
    public void bigDecimalCalculation_usingConstructorMethod() {
        BigDecimal x1 = new BigDecimal(0.3);
        BigDecimal x2 = new BigDecimal(0.2);
        BigDecimal x3 = new BigDecimal(0.11);

        BigDecimal actual = x1.add(x2);
        actual = actual.add(x3);
        BigDecimal expected = new BigDecimal(0.61);

        assertThat(actual).isNotEqualByComparingTo(expected);
    }

    @Test
    public void bigDecimalCalculation_usingValueOfMethod() {
        BigDecimal x1 = BigDecimal.valueOf(0.3);
        BigDecimal x2 = BigDecimal.valueOf(0.2);
        BigDecimal x3 = BigDecimal.valueOf(0.11);

        BigDecimal actual = x1.add(x2);
        actual = actual.add(x3);
        BigDecimal expected = BigDecimal.valueOf(0.61);

        assertThat(actual).isEqualByComparingTo(expected);
    }
}
