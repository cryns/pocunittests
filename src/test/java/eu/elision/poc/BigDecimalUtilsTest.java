package eu.elision.poc;

import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class BigDecimalUtilsTest {

    @Test
    public void isZero_whereInputIsNull() {
        assertThat(BigDecimalUtils.isZero(null)).isEqualTo(true);
    }

    @Test
    public void isZero_whereInputIsZero() {
        assertThat(BigDecimalUtils.isZero(BigDecimal.ZERO)).isEqualTo(true);
    }

    @Test
    public void isZero_whereInputIsNotZero() {
        assertThat(BigDecimalUtils.isZero(BigDecimal.TEN)).isEqualTo(false);
    }

    @Test
    public void isNotZero_whereInputIsNull() {
        assertThat(BigDecimalUtils.isNotZero(null)).isEqualTo(false);
    }

    @Test
    public void isNotZero_whereInputIsZero() {
        assertThat(BigDecimalUtils.isNotZero(BigDecimal.ZERO)).isEqualTo(false);
    }

    @Test
    public void isNotZero_whereInputIsNotZero() {
        assertThat(BigDecimalUtils.isNotZero(BigDecimal.ONE)).isEqualTo(true);
    }

    @Test
    public void roundHalfUp_toOneDigit() {
        BigDecimal expected = BigDecimal.valueOf(55.5);
        BigDecimal actual = BigDecimalUtils.roundHalfUp(BigDecimal.valueOf(55.45), 1);

        assertThat(actual).isEqualByComparingTo(expected);
    }
}